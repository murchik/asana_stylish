# asana_stylish

Greasemonkey userscript to fix Asana's hidden project tasks list with custom CSS (e.g. Stylish) applied

# What's this for

If you're using custom style sheets that change background colors of all elements (e.g. to have w/b palette) with Asana, you will quickly find out that list of tasks in projects are invisible for you.

Since Asana's support is being a dick and they never solve their customers' problems, this is a tiny workaround.

# How to use

Just add this userscript to your Greasemonkey / Scriptish / whatever user script engine you're using and it'll solve all the issues.

# Alternative solution

Add this selector to your custom CSS:

```css
.react-mount-node .DragLayer {
    display: none;
}
```
